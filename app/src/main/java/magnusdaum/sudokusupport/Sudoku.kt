package magnusdaum.sudokusupport

import kotlin.math.sqrt

class Sudoku {
    val mSideLen = 9
    val mSize = mSideLen * mSideLen
    val mBoard = IntArray(mSize)

    fun solve(): Boolean {
        return isSolvable() && solveRecursive(0)
    }

    fun isSolvable(): Boolean {
        for ((idx, value) in mBoard.withIndex().filter { it.value != 0 }) {
            for (neighborIdx in getNeighborIndices(idx)) {
                if (mBoard[neighborIdx] == value) {
                    return false
                }
            }
        }
        return true
    }

    fun getCandidates(idx: Int): IntArray {
        // get possible new values for given board position
        val candidates = IntArray(mSideLen + 1) { it }
        for (neighborIdx in getNeighborIndices(idx)) {
            candidates[mBoard[neighborIdx]] = 0
        }
        return candidates.filter { it > 0 }.toIntArray()
    }

    fun isValid(idx: Int, value: Int): Boolean {
        return (0 == value) || (value in getCandidates(idx))
    }

    operator fun get(idx: Int): Int {
        return mBoard[idx]
    }

    operator fun set(idx: Int, value: Int) {
        if (isValid(idx, value)) {
            mBoard[idx] = value
        }
    }

    override fun toString(): String {
        var outStr = ""
        val cellLen = sqrt(mSideLen.toFloat()).toInt()
        for ((idx, value) in mBoard.withIndex()) {
            if (idx % mSideLen == 0) {
                outStr += "\n"
            }
            if (idx % cellLen == 0) {
                outStr += "|"
            }
            outStr += (if (value > 0) value.toString() else " ") + " "
        }
        return outStr
    }

    ///////////////////////////////////////////////////////////////////////////
    // private methods
    ///////////////////////////////////////////////////////////////////////////

    private fun getNeighborIndices(idx: Int): IntArray {
        // find all board indices that affect current position
        val neighborIndices = ArrayList<Int>()
        val row = idx / mSideLen
        val col = idx % mSideLen
        // add from row and column
        neighborIndices.addAll(row * mSideLen until (row + 1) * mSideLen)
        neighborIndices.addAll(col until mBoard.size step mSideLen)
        // add from cell
        val cellLen = sqrt(mSideLen.toDouble()).toInt()
        val cellRow = (row / cellLen) * cellLen
        val cellCol = (col / cellLen) * cellLen
        neighborIndices.addAll((0 until mSideLen).map {
            (cellRow + (it / cellLen)) * mSideLen + cellCol + (it % cellLen)
        })
        // return filtered indices
        return neighborIndices.distinct().filter { it != idx }.toIntArray()
    }

    private fun solveRecursive(_idx: Int): Boolean {
        var idx = _idx
        while ((idx < mBoard.size) && mBoard[idx] > 0) {
            idx += 1
        }
        if (idx < mBoard.size) {
            for (candidate in getCandidates(idx)) {
                mBoard[idx] = candidate
                if (solveRecursive(idx + 1)) {
                    return true
                }
            }
            mBoard[idx] = 0
        }
        return (idx == mBoard.size)
    }
}