package magnusdaum.sudokusupport

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG: String = "SudokuSupport"
        private const val REQUEST_CODE_PERMISSIONS = 0xABBA
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        // TODO how to listen to keyevents in appropriate view instead?
        val c = event.unicodeChar.toChar()
        val value = if (c.isDigit()) {
            c - '0'
        } else {
            0
        }
        (sudokuFragment as SudokuFragment).setBoardValue(value)
        return super.onKeyUp(keyCode, event)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (cameraFragment as CameraFragment).setOnImageCapturedListener(object :
            CameraFragment.OnImageCapturedListener {
            override fun onImageCaptured(image: Bitmap) {
                (imageDisplayFragment as ImageDisplayFragment).setImage(image)
                switchToImageDisplay()
            }
        })
        (imageDisplayFragment as ImageDisplayFragment).setOnImageApprovedListener(object :
            ImageDisplayFragment.OnImageApprovedListener {
            override fun onImageApproved(image: Bitmap) {
                (sudokuFragment as SudokuFragment).setImage(image)
                switchToSudoku()
            }

            override fun onImageDiscarded() {
                switchToCameraCapture()
            }
        })
        (sudokuFragment as SudokuFragment).setOnSudokuFragmentListener(object :
            SudokuFragment.OnSudokuFragmentListener {
            override fun onCameraButtonClicked() {
                switchToCameraCapture()
            }

            override fun onImageSelected(image: Bitmap) {
                (imageDisplayFragment as ImageDisplayFragment).setImage(image)
                switchToImageDisplay()
            }
        })

        switchToSudoku()
    }

    override fun onBackPressed() {
        if (cameraFragment.isVisible) {
            switchToSudoku()
        } else if (sudokuFragment.isVisible) {
            switchToCameraCapture()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            grantResults.forEachIndexed { idx, result ->
                if (result == PackageManager.PERMISSION_GRANTED) {
                    when (permissions[idx]) {
                        Manifest.permission.CAMERA -> switchToCameraCapture()
                        else -> {
                            Log.e(TAG, "unknown permission received: " + permissions[idx])
                        }
                    }
                    return
                }
            }
            Toast.makeText(
                baseContext,
                resources.getString(R.string.app_permissions_denied),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    // TODO handle fragments appropriately

    private fun switchToCameraCapture() {
        Log.d(TAG, "switch to camera capture")
        if (!hasPermission(Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_CODE_PERMISSIONS
            )
            return
        }
        (cameraFragment as CameraFragment).startCamera()
        supportFragmentManager.beginTransaction().hide(sudokuFragment).commit()
        supportFragmentManager.beginTransaction().hide(imageDisplayFragment).commit()
        supportFragmentManager.beginTransaction().show(cameraFragment).commit()
    }

    private fun switchToImageDisplay() {
        Log.d(TAG, "switch to image display")
        supportFragmentManager.beginTransaction().hide(cameraFragment).commit()
        supportFragmentManager.beginTransaction().hide(sudokuFragment).commit()
        supportFragmentManager.beginTransaction().show(imageDisplayFragment).commit()
    }

    private fun switchToSudoku() {
        Log.d(TAG, "switch to sudoku")
        supportFragmentManager.beginTransaction().hide(cameraFragment).commit()
        supportFragmentManager.beginTransaction().hide(imageDisplayFragment).commit()
        supportFragmentManager.beginTransaction().show(sudokuFragment).commit()
    }

    private fun hasPermission(permission: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(
            baseContext,
            permission
        )
    }
}