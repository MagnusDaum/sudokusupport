package magnusdaum.sudokusupport

import android.util.Log
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizer
import kotlin.math.max
import kotlin.math.min


private class OCRDigit {
    var digit: Char = 0.toChar()
    var left: Int = 0
    var top: Int = 0
    var right: Int = 0
    var bottom: Int = 0
}


class SudokuBuilder {
    private var mSudoku = Sudoku()
    private var mListener: Listener? = null

    interface Listener {
        fun onSudokuUpdated()
    }

    fun recognizeImage(image: InputImage) {
        Log.d(MainActivity.TAG, "recognizeImage")
        val recognizer: TextRecognizer = TextRecognition.getClient()
        recognizer.process(image)
            .addOnSuccessListener { texts ->
                val ocrList = parseRecognizedText(texts as Text)
                addDigits(ocrList)
                mListener?.onSudokuUpdated()
            }
            .addOnFailureListener { e ->
                e.printStackTrace()
            }
    }

    fun setListener(listener: Listener) {
        mListener = listener
    }

    fun build(): Sudoku {
        return mSudoku
    }

    fun reset() {
        mSudoku = Sudoku()
        mListener?.onSudokuUpdated()
    }

    ///////////////////////////////////////////////////////////////////////////
    // private methods
    ///////////////////////////////////////////////////////////////////////////

    private fun addDigits(ocrList: ArrayList<OCRDigit>) {
        Log.i(MainActivity.TAG, "unfiltered ocrs: " + ocrList.size)
        if (ocrList.size == 0) return
        // determine board boundary
        var left: Int = Int.MAX_VALUE;
        var top = Int.MAX_VALUE;
        var right = 0;
        var bottom = 0;
        for (d in ocrList) {
            left = min(left, d.left)
            top = min(top, d.top)
            right = max(right, d.right)
            bottom = max(bottom, d.bottom)
        }
        Log.i(MainActivity.TAG, "ocr bbox: " + (right - left) + "x" + (bottom - top))
        // add digits to board
        val wCell = (right - left) / 9
        val hCell = (bottom - top) / 9
        for (d in ocrList) {
            val value = d.digit.toInt() - '0'.toInt()
            val col: Int = ((d.left + d.right) / 2 - left) / wCell
            val row: Int = ((d.top + d.bottom) / 2 - top) / hCell
            val idx = row * 9 + col
            if (mSudoku.isValid(idx, value)) {
                mSudoku[idx] = value
            }
        }
        Log.i(MainActivity.TAG, "num board entries: " + mSudoku.mBoard.count { it > 0 })
    }

    private fun parseRecognizedText(text: Text): ArrayList<OCRDigit> {
        val ocrList = ArrayList<OCRDigit>()
        val resultText = text.text
        Log.i(MainActivity.TAG, "text: " + resultText.replace('\n', ' '))
        for (block in text.textBlocks) {
            for (line in block.lines) {
                for (element in line.elements) {
                    val elementText = element.text
                    val elementFrame = element.boundingBox
                    if (elementFrame != null) {
                        val s = elementText.filter { it.isDigit() }
                        if (s.isNotEmpty()) {
                            val w = (elementFrame.right - elementFrame.left) / s.length
                            for (idx in s.indices) {
                                val d = OCRDigit()
                                d.digit = s[idx];
                                d.left = elementFrame.left + w * idx
                                d.top = elementFrame.top
                                d.right = elementFrame.left + w * (idx + 1)
                                d.bottom = elementFrame.bottom
                                ocrList.add(d)
                                Log.d(
                                    MainActivity.TAG,
                                    "ocr: " + d.left + ", " + d.top + ": " + d.digit
                                )
                            }
                        }
                    }

                }
            }
        }
        return ocrList
    }
}
