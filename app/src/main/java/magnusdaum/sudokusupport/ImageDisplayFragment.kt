package magnusdaum.sudokusupport

import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_image_display.*
import kotlin.math.*


class ImageHandler(private val image: Bitmap?) {
    private var left = 0.0F
    private var top = 0.0F
    private var width = 1.0F
    private var height = 1.0F
    private var rotationDeg = 0.0F

    fun scale(factor: Float) {
        val oldWidth = width
        val oldHeight = height
        width = min(1.0F, width / factor)
        height = min(1.0F, height / factor)
        val diffX = (oldWidth - width) / 2
        val diffY = (oldHeight - height) / 2
        // do not call move() as this move is rotation independent (is it?)
        left = min(1.0F - width, max(0.0F, left + diffX))
        top = min(1.0F - height, max(0.0F, top + diffY))
    }

    fun move(x: Float, y: Float) {
        val translation = rotatePosition(x, y, -rotationDeg)
        left = min(1.0F - width, max(0.0F, left - translation.first))
        top = min(1.0F - height, max(0.0F, top - translation.second))
    }

    fun rotate(rotationDegree: Float) {
        rotationDeg += rotationDegree
    }

    fun getSelection(): Bitmap? {
        if (image == null) return null
        val refWidth = image.width
        val refHeight = image.height
        val roi = Rect(
            (left * refWidth).toInt(),
            (top * refHeight).toInt(),
            ((left + width) * refWidth).toInt(),
            ((top + height) * refHeight).toInt()
        )
        val subImg = Bitmap.createBitmap(image, roi.left, roi.top, roi.width(), roi.height())
        val rotMat = Matrix()
        rotMat.setRotate(rotationDeg)
        return Bitmap.createBitmap(subImg, 0, 0, subImg.width, subImg.height, rotMat, false)
    }

    private fun rotatePosition(x: Float, y: Float, angle: Float): Pair<Float, Float> {
        // is there no nicer way to do this?
        val rotMat = Matrix()
        rotMat.setRotate(angle)
        val array = floatArrayOf(x, y)
        rotMat.mapPoints(array)
        return Pair(array[0], array[1])
    }
}

private class CustomGestureDetector(private val mListener: OnCustomGestureListener) {
    private var mPointerId0 = MotionEvent.INVALID_POINTER_ID
    private var mPos0 = Point2D()
    private var mPointerId1 = MotionEvent.INVALID_POINTER_ID
    private var mPos1 = Point2D()
    private var mMovePos = Point2D()

    interface OnCustomGestureListener {
        fun onChange(x: Float, y: Float, angleDeg: Float, scale: Float)
    }

    private data class Point2D(var x: Float = 0.0F, var y: Float = 0.0F) {
        fun angleTo(p: Point2D): Float {
            return atan2(p.y - y, p.x - x)
        }

        fun distanceTo(p: Point2D): Point2D {
            return Point2D(p.x - x, p.y - y)
        }

        fun norm(): Float {
            return sqrt(x.pow(2) + y.pow(2))
        }
    }

    fun onTouchEvent(event: MotionEvent): Boolean {
        val pointerId = event.getPointerId(event.actionIndex)
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                mPointerId0 = pointerId
                mPos0 = getPosition(event, mPointerId0)
                mMovePos = mPos0.copy()
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                if (mPointerId1 == MotionEvent.INVALID_POINTER_ID) {
                    mPointerId1 = pointerId
                    mPos1 = getPosition(event, mPointerId1)
                    mPos0 = getPosition(event, mPointerId0)  // fixate reference angle
                }
            }
            MotionEvent.ACTION_MOVE -> {
                val curPos = getPosition(event, pointerId)
                val moveDistance = mMovePos.distanceTo(curPos)
                mMovePos = curPos

                var angleDeg = 0.0F
                var scale = 1.0F
                if (mPointerId1 != MotionEvent.INVALID_POINTER_ID) {
                    val curPos0 = getPosition(event, mPointerId0)
                    val curPos1 = getPosition(event, mPointerId1)

                    val dist0 = mPos0.distanceTo(mPos1).norm()
                    val dist1 = curPos0.distanceTo(curPos1).norm()
                    scale = dist1 / dist0
                    val angle0 = mPos0.angleTo(mPos1)
                    val angle1 = curPos0.angleTo(curPos1)
                    angleDeg = toDegree(angle1 - angle0)

                    mPos0 = curPos0
                    mPos1 = curPos1
                }
                //angleDeg = 0.0F // TODO remove to enable rotation
                mListener.onChange(moveDistance.x, moveDistance.y, angleDeg, scale)
            }
            MotionEvent.ACTION_POINTER_UP -> {
                if (pointerId == mPointerId0) {
                    mPointerId0 = mPointerId1
                    mPos0 = mPos1.copy()
                    mPointerId1 = MotionEvent.INVALID_POINTER_ID
                    mMovePos = mPos0.copy()
                } else if (pointerId == mPointerId1) {
                    mPointerId1 = MotionEvent.INVALID_POINTER_ID
                    // TODO would it make sense to switch to other active pointers instead?
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                mPointerId0 = MotionEvent.INVALID_POINTER_ID
            }
        }
        return true
    }

    private fun getPosition(event: MotionEvent, pointerId: Int): Point2D {
        if (pointerId == MotionEvent.INVALID_POINTER_ID) {
            return Point2D()
        }
        val pointerIndex = event.findPointerIndex(pointerId)
        val x = event.getX(pointerIndex)
        val y = event.getY(pointerIndex)
        return Point2D(x, y)
    }

    private fun toDegree(angleRad: Float): Float {
        var angleDeg = (angleRad * 360.0F / PI.toFloat()) % 360.0F
        if (angleDeg <= -180.0F) angleDeg += 360.0F
        if (angleDeg > 180.0F) angleDeg -= 360.0F
        return angleDeg
    }
}

class ImageDisplayFragment : Fragment(), View.OnTouchListener {
    private var mImageHandler = ImageHandler(null)
    private lateinit var mGestureDetector: CustomGestureDetector
    private var mImageRoiApprovedListener: OnImageApprovedListener? = null
    private val mSelectionRoiDrawable = ShapeDrawable(RectShape())

    interface OnImageApprovedListener {
        fun onImageApproved(image: Bitmap)
        fun onImageDiscarded()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mGestureDetector =
            CustomGestureDetector(object : CustomGestureDetector.OnCustomGestureListener {
                override fun onChange(x: Float, y: Float, angleDeg: Float, scale: Float) {
                    val relX = x / view!!.width
                    val relY = y / view!!.height
                    Log.d(MainActivity.TAG, "rot $angleDeg | scale $scale | $relX, $relY")

                    mImageHandler.move(relX, relY)
                    mImageHandler.scale(scale)
                    mImageHandler.rotate(angleDeg)

                    displayImage()
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image_display, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        acceptImageButton.setOnClickListener { approveImage() }
        discardImageButton.setOnClickListener { discardImage() }
        rotateImageButton.setOnClickListener {
            mImageHandler.rotate(90.0F)
            displayImage()
        }

        view.setOnTouchListener(this)

        mSelectionRoiDrawable.paint.style = Paint.Style.STROKE
        mSelectionRoiDrawable.paint.color = Color.RED
    }

    override fun onTouch(v: View?, event: MotionEvent): Boolean {
//        Log.d(MainActivity.TAG, "on touch " + event.actionMasked + " " + event.actionIndex)
        mGestureDetector.onTouchEvent(event)
        return true
    }

    private fun ShapeDrawable.scale(refWidth: Int, refHeight: Int) {
        val sideLen = (min(refWidth, refHeight) * 0.8F).toInt()
        val left = refWidth / 2 - sideLen / 2
        val top = refHeight / 2 - sideLen / 2
        this.bounds = Rect(left, top, left + sideLen, top + sideLen)
        this.paint.strokeWidth = sideLen / 200.0F
        this.paint.pathEffect = DashPathEffect(floatArrayOf(sideLen / 40.0F, sideLen / 20.0F), 0f)
    }

    private fun displayImage() {
        Log.d(MainActivity.TAG, "displayimg view " + view?.width + "x" + view?.height)
        var subImage = mImageHandler.getSelection() ?: return
        subImage = subImage.copy(subImage.config, true)

        val canvas = Canvas(subImage)
        Log.d(MainActivity.TAG, "displayimg canvas " + canvas.width + "x" + canvas.height)
        mSelectionRoiDrawable.scale(canvas.width, canvas.height)
        mSelectionRoiDrawable.draw(canvas)

        capturedImageView.setImageBitmap(subImage)
    }

    fun setOnImageApprovedListener(listener: OnImageApprovedListener) {
        mImageRoiApprovedListener = listener
    }

    fun setImage(image: Bitmap?) {
        mImageHandler = ImageHandler(image)
        displayImage()
    }

    private fun approveImage() {
        val bmp: Bitmap = (capturedImageView.drawable as BitmapDrawable).bitmap
        val roi = mSelectionRoiDrawable.bounds
        val subBmp = Bitmap.createBitmap(bmp, roi.left, roi.top, roi.width(), roi.height())
        Log.i(
            MainActivity.TAG,
            "image cropped: " + bmp.width + "x" + bmp.height + " -> " + subBmp.width + "x" + subBmp.height
        )
        // according to https://developers.google.com/ml-kit/vision/text-recognition/android#input-image-guidelines
        // characters should be 16-24 px. so maybe downscale image for hopefully better recognition
        // TODO mlkit will do this by itself, right?
//        val scale = (9 + 1) * 24.0F / min(subBmp.width, subBmp.height)  // 9 cells per direction (+1 for leeway)
//        val bmpScaled = Bitmap.createScaledBitmap(subBmp, (subBmp.width * scale).toInt(), (subBmp.height * scale).toInt(), true)
//        Log.i(TAG, "image scaled from " + subBmp.width + "x" + subBmp.height + " -> " + bmpScaled.width + "x" + bmpScaled.height)

        mImageRoiApprovedListener?.onImageApproved(subBmp)
    }

    private fun discardImage() {
        setImage(null)
        mImageRoiApprovedListener?.onImageDiscarded()
    }
}