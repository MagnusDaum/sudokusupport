package magnusdaum.sudokusupport

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import com.google.mlkit.vision.common.InputImage
import kotlinx.android.synthetic.main.fragment_sudoku.*


class SudokuFragment : Fragment() {
    private val mSudokuBuilder = SudokuBuilder()
    private var mSudoku = Sudoku()
    private var mSudokuFragmentListener: OnSudokuFragmentListener? = null

    companion object {
        const val IMAGE_PICK_REQUEST_CODE = 54075
    }

    interface OnSudokuFragmentListener {
        fun onCameraButtonClicked()
        fun onImageSelected(image: Bitmap)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sudoku, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cameraButton.setOnClickListener {
            mSudokuFragmentListener?.onCameraButtonClicked()
        }

        sudokuSolveButton.setOnClickListener {
            Log.d(MainActivity.TAG, "solving...")
            if (!mSudoku.solve()) {
                Toast.makeText(
                    context,
                    resources.getString(R.string.sudoku_not_solved),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                sudokuView.updateBoard(mSudoku)
            }
        }

        sudokuClearButton.setOnClickListener {
            mSudokuBuilder.reset()
        }

        loadImageButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK).setType("image/*")
            startActivityForResult(
                Intent.createChooser(intent, "Select a file"),
                IMAGE_PICK_REQUEST_CODE
            )
        }

        sudokuView.setOnTouchListener(sudokuView)

        mSudokuBuilder.setListener(object : SudokuBuilder.Listener {
            override fun onSudokuUpdated() {
                mSudoku = mSudokuBuilder.build()
                Log.i(MainActivity.TAG, "board: $mSudoku")
                sudokuView.setBoard(mSudoku)
                sudokuSolveButton.isEnabled = mSudoku.isSolvable()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_PICK_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            val fileUri = data?.data ?: return
            val bitmap = loadBitmapFromFile(fileUri)
            bitmap?.let { mSudokuFragmentListener?.onImageSelected(bitmap) }
        }
    }

    fun setOnSudokuFragmentListener(listener: OnSudokuFragmentListener) {
        mSudokuFragmentListener = listener
    }

    fun setBoardValue(value: Int) {
        val idx = sudokuView.getCurrentIndex()
        if (idx >= 0) {
            mSudoku[idx] = value
            sudokuView.setBoardValue(value)  // TODO unify board handling
        }
    }

    fun setImage(bmp: Bitmap) {
        Log.d(MainActivity.TAG, "set image " + bmp.width + ", " + bmp.height)
        val img = InputImage.fromBitmap(bmp, 0)
        sudokuSolveButton.isEnabled = false
        mSudokuBuilder.recognizeImage(img)
    }

    private fun loadBitmapFromFile(fileUri: Uri): Bitmap? {
        val imageStream = context?.contentResolver?.openInputStream(fileUri)
        var bitmap: Bitmap? = BitmapFactory.decodeStream(imageStream)
        Log.d(MainActivity.TAG, "bitmap loaded: " + bitmap?.width + " x " + bitmap?.height)
        if (bitmap != null) {
            val exif = ExifInterface(imageStream!!)
            val orientation =
                exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED
                )
            val rotInDeg = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> 90
                ExifInterface.ORIENTATION_ROTATE_180 -> 180
                ExifInterface.ORIENTATION_ROTATE_270 -> 270
                else -> 0
            }
            Log.d(MainActivity.TAG, "bitmap rotate by: $rotInDeg")
            if (rotInDeg != 0) {
                val rotMat = Matrix()
                rotMat.setRotate(rotInDeg.toFloat())
                bitmap =
                    Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, rotMat, true)
            }
        }
        return bitmap
    }
}