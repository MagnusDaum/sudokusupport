package magnusdaum.sudokusupport

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.fragment_sudoku.view.*
import java.lang.Float.max
import java.lang.Float.min


internal class SudokuView(context: Context?, attrs: AttributeSet?) :
    View(context, attrs), View.OnTouchListener {
    private var painter: Paint = Paint()
    private val N_LINES = 10
    private var gridLines = FloatArray(4 * 2 * N_LINES) { 0.0F }
    private var mSudoku = Sudoku()
    private var origIndices = BooleanArray(mSudoku.mSize)
    private var mCurRow = -1
    private var mCurCol = -1

    init {
        val stepSize = 1.0f / (N_LINES - 1)
        for (idx in 0 until N_LINES) {
            val pos: Float = max(0.01f, min(stepSize * idx, 0.99f))
            // horizontal lines
            var offset = idx * 8
            gridLines[offset] = 0.01f
            gridLines[offset + 1] = pos
            gridLines[offset + 2] = 0.99f
            gridLines[offset + 3] = pos
            // vertical lines
            offset += 4
            gridLines[offset] = pos
            gridLines[offset + 1] = 0.01f
            gridLines[offset + 2] = pos
            gridLines[offset + 3] = 0.99f
        }
    }

    fun updateBoard(board: Sudoku) {
        mSudoku = board
        invalidate()
    }

    fun setBoard(board: Sudoku) {
        origIndices = BooleanArray(board.mSize) { board[it] > 0 }
        Log.i(MainActivity.TAG, "orig indices " + origIndices.sumBy { if (it) 1 else 0 })
        updateBoard(board)
    }

    fun setBoardValue(value: Int) {
        val idx = sudokuView.getCurrentIndex()
        if (mSudoku.isValid(idx, value)) {
            mSudoku[idx] = value
            origIndices[idx] = false
            invalidate()
        }
    }

    fun getCurrentIndex(): Int {
        return if ((mCurRow >= 0) && (mCurCol >= 0)) {
            mCurRow * mSudoku.mSideLen + mCurCol
        } else {
            -1
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val boardWidth = width.toFloat()
        val boardHeight = height.toFloat()
        val sideLen = mSudoku.mSideLen
        val colWidth = boardWidth / sideLen
        val rowHeight = boardHeight / sideLen

        // draw grid
        painter.color = Color.BLACK
        painter.style = Paint.Style.STROKE
        val lines = gridLines.copyOf()
        for (lineIdx in lines.indices step 4) {
            painter.strokeWidth = if ((lineIdx / 8.0).toInt() % 3 == 0) 4.0F else 1.0F
            canvas.drawLine(
                lines[lineIdx] * boardWidth,
                lines[lineIdx + 1] * boardHeight,
                lines[lineIdx + 2] * boardWidth,
                lines[lineIdx + 3] * boardHeight,
                painter
            )
        }
        canvas.drawLines(lines, painter)

        // draw selected cell
        if ((mCurRow >= 0) && (mCurCol >= 0)) {
            painter.color = Color.BLUE
            painter.strokeWidth = rowHeight / 20.0F
            painter.style = Paint.Style.STROKE
            canvas.drawRect(
                mCurCol * colWidth,
                mCurRow * rowHeight,
                (mCurCol + 1) * colWidth,
                (mCurRow + 1) * rowHeight,
                painter
            )
        }

        // draw numbers
        painter.textSize = rowHeight * 0.9F
        painter.textAlign = Paint.Align.CENTER
        painter.style = Paint.Style.FILL
        for (row in 0 until sideLen) {
            for (col in 0 until sideLen) {
                val boardIdx = row * sideLen + col
                val value: Int = mSudoku[boardIdx]
                if (value != 0) {
                    painter.color = if (origIndices[boardIdx]) Color.BLACK else Color.GRAY
                    canvas.drawText(
                        value.toString(),
                        col * colWidth + colWidth / 2,
                        (row) * rowHeight + rowHeight / 2 - (painter.descent() + painter.ascent()) / 2,
                        painter
                    )
                }
            }
        }
    }

    override fun onTouch(p0: View?, event: MotionEvent): Boolean {
        if (event.actionMasked == MotionEvent.ACTION_UP) {
            val col = getCol(event.x)
            val row = getCol(event.y)
            if ((col != mCurCol) || (row != mCurRow)) {
                mCurCol = col
                mCurRow = row
                showSoftKeyboard()
            } else {
                mCurCol = -1
                mCurRow = -1
            }

            this.invalidate()
        }
        return true
    }

    private fun showSoftKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(rootView, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun getCol(x: Float): Int {
        return (x / width.toFloat() * mSudoku.mSideLen).toInt()
    }

    private fun getRow(y: Float): Int {
        return (y / height.toFloat() * mSudoku.mSideLen).toInt()
    }
}